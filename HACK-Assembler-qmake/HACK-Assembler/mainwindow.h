#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include    <QMainWindow>
#include    <string.h>
#include    <string>
#include    <QDirIterator>
#include    "assembler/Assembler.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();

    void on_actionOpen_File_triggered();

    void on_actionSave_asm_File_triggered();

    void on_actionSave_As_asm_File_triggered();

    void on_actionNew_File_triggered();

    void on_pushButton_assemble_clicked();

    void on_pushButton_detect_clicked();

    void on_pushButton_flash_clicked();

private:
    Ui::MainWindow *ui;

    QString  locateFile(QString dir, QString find);
};
#endif // MAINWINDOW_H
