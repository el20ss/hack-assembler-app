//
// Created by shraw on 2/11/2023.
//

#ifndef HACK_ASSEMBLER_C_ASSEMBLER_H
#define HACK_ASSEMBLER_C_ASSEMBLER_H

#define     REG_COUNT   8
#define     REG_SIZE    5

#define     JMP_COUNT   8
#define     JMP_SIZE    5

#define     COMP_COUNT   10
#define     COMP_SIZE    1

#include    "Lexer.hpp"
#include    "SymbolTable.hpp"
#include    "CodeGenerator.hpp"
#include    <iostream>
#include    <string>

extern  const   char* registers[];
extern  const   char* jumpIns[];
extern  const   char* compIns[];

class Assembler{

private:
    Lexer*  lexer;
    SymbolTable*    symboltable;
    CodeGenerator*  codegen;

public:
    explicit Assembler(string fname);
    ~Assembler();

    int     parse();
    int     parse_first();
    int     parse_second();

    int     is_A_ins();
    int     is_C_ins();

    static int     isRegister(char* reg){

        for(int i = 0; i < REG_COUNT; i++){
            if(!strcmp(registers[i],reg)){
                return 0;
            }
        }
        return 1;
    }

    static int     isComputation(char* comp){

        for(int i = 0; i < COMP_COUNT; i++){
            if(!strcmp(comp,compIns[i])){
                return 0;
            }
        }
        return 1;
    }

    static int     isJump(char* jump){

        for(int i = 0; i < JMP_COUNT; i++){
            if(!strcmp(jump,jumpIns[i])){
                return 0;
            }
        }
        return 1;
    }

};


#endif //HACK_ASSEMBLER_C_ASSEMBLER_H
