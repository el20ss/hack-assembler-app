//
// Created by shraw on 2/5/2023.
//

#include "Lexer.hpp"

/*=============================================================================================================
                                            CORE METHODS
==============================================================================================================*/


Lexer::Lexer   (const char* fname){

    this->filename    =   fopen(fname,"r");
    this->tokens      =   (Token*) malloc(sizeof(Token));
    this->tokenCount  =   0;
    this->nextToken   =   0;

    if(this->tokens == nullptr){
        throw std::invalid_argument("Failed to create object : Lexer");
    }

}

Lexer::~Lexer   (){

    fclose(this->filename);
    this->filename    =   nullptr;

    for(int i = 0; i < this->tokenCount; i++){
        free(this->tokens[i].string);
    }

    free(tokens);
    this->tokenCount  =   0;
    this->nextToken   =   0;
}

int Lexer::tokenize() {

    char    tmp[2]      =   "\0";

    Token   t;
    t.string        =   (char*) malloc(sizeof(char)*STR_SIZE);
    int     index   =   0;

    while(!feof(this->filename) && this->filename != nullptr){
        tmp[0]  =   fgetc(this->filename);

        //if comment
        if(tmp[0] == '/'){
            tmp[0]  = fgetc(this->filename);
            if(tmp[0] == '/'){
                while(tmp[0] != '\n' && !feof(this->filename)){
                    tmp[0]  = fgetc(this->filename);
                }
            }
        }

        //if symbol
        if(!isSymbol(&tmp[0]))  {
            t.type      =   SYMBOL;

            t.string[index++]   =   tmp[0];
            tmp[0]  = fgetc(this->filename);

            while(!isSymbol(&tmp[0]) || !isNumeric(&tmp[0]) || tmp[0] == '_'){
                t.string[index++]   =   tmp[0];
                tmp[0]  = fgetc(this->filename);
            }
            //append token
            t.string[index++]   =   '\0';
            appendToken(t);

            //free string
            free(t.string);
            t.string        =   (char*) malloc(sizeof(char)*STR_SIZE);
            index   =   0;
        }

        //if numeric
        if(!isNumeric(&tmp[0])) {
            t.type      =   VALUE;

            t.string[index++]   =   tmp[0];
            tmp[0]  = fgetc(this->filename);

            while(!isNumeric(&tmp[0])){
                t.string[index++]   =   tmp[0];
                tmp[0]  = fgetc(this->filename);
            }

            //append token
            t.string[index++]   =   '\0';
            appendToken(t);

            //free string
            free(t.string);
            t.string        =   (char*) malloc(sizeof(char)*STR_SIZE);
            index   =   0;
        }

        //if operator
        if(!isOperator(&tmp[0])){
            t.type      =   OPERATOR;

            t.string[index++]   =   tmp[0];

            //append token
            t.string[index++]   =   '\0';
            appendToken(t);

            //free string
            free(t.string);
            t.string        =   (char*) malloc(sizeof(char)*STR_SIZE);
            index   =   0;
        }
    }

    strcpy(t.string,"END");
    t.type      =   EOF;
    appendToken(t);

    free(t.string);
    return 0;
}

int Lexer::appendToken (Token t) {

    tokenCount++;
    //if realloc fails, attempt to manually allocate memory
    auto*  tmp = (Token *) malloc(sizeof(Token) * tokenCount);

    if(tmp == nullptr){
        //memory alloc failed
        return 1;
    }

    for (int i = 0; i < tokenCount - 1; i++) {
        tmp[i].string   =   (char *) malloc(sizeof(char)*STR_SIZE);
        strcpy(tmp[i].string,tokens[i].string);
        tmp[i].type     =   tokens[i].type;
    }

    for (int i = 0; i < tokenCount - 1; i++) {
        free(tokens[i].string);
    }
    free(tokens);

    tmp[tokenCount - 1].string  = (char *) malloc(sizeof(char)*STR_SIZE);
    strcpy(tmp[tokenCount - 1].string, t.string);
    tmp[tokenCount - 1].type    = t.type;

    tokens    =   tmp;
    return 0;
}



