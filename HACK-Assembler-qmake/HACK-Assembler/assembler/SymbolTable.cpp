//
// Created by shraw on 2/9/2023.
//

#include "SymbolTable.hpp"

/*=============================================================================================================
                                            CORE METHODS
==============================================================================================================*/


SymbolTable::SymbolTable() {

    try {
        symbolTable     =   (Symbol*)   malloc(sizeof(Symbol)*PREFILL_SYMBOL);
    }catch (const bad_alloc& e) {
        throw std::invalid_argument("Failed to create object : SymbolTable");
    }

    for(int i = 0; i < PREFILL_SYMBOL; i++){
        try {
            symbolTable[i].symbol   =   (char*) malloc(sizeof(char)*STR_SIZE);
        }catch (const bad_alloc& e) {
            throw std::invalid_argument("Failed to create object : SymbolTable");
        }
    }

    symbolCount     =   PREFILL_SYMBOL;
    nextAddress     =   NEXT_ADDR;

    //Prefill symboltable
    for(int i = 0; i < 10; i++){
        symbolTable[i].symbol[0]    =   'R';
        symbolTable[i].symbol[1]    =   i + '0';
        symbolTable[i].symbol[2]    =   '\0';

        symbolTable[i].address      =   i;
    }

    for(int i = 0; i < 6; i++){
        symbolTable[10+i].symbol[0]    =   'R';
        symbolTable[10+i].symbol[1]    =   '1';
        symbolTable[10+i].symbol[2]    =   i + '0';
        symbolTable[10+i].symbol[3]    =   '\0';

        symbolTable[10+i].address      =   10 + i;
    }

    strcpy(symbolTable[17].symbol,"SP\0");      symbolTable[17].address =   0;
    strcpy(symbolTable[18].symbol,"LCL\0");     symbolTable[18].address =   1;
    strcpy(symbolTable[19].symbol,"ARG\0");     symbolTable[19].address =   2;
    strcpy(symbolTable[20].symbol,"THIS\0");    symbolTable[20].address =   3;
    strcpy(symbolTable[21].symbol,"THAT\0");    symbolTable[21].address =   4;
    strcpy(symbolTable[22].symbol,"SCREEN\0");  symbolTable[22].address =   16384;
    strcpy(symbolTable[23].symbol,"KBO\0");     symbolTable[23].address =   24576;
}

SymbolTable::~SymbolTable() {

    for(int i = 0; i < symbolCount; i++){
        free(symbolTable[i].symbol);
    }
    free(symbolTable);

    symbolCount =   0;
    nextAddress =   0;

}

Symbol* SymbolTable::appendSymbol(char* s) {

    symbolCount++;
    Symbol *  tmp;
    //if realloc fails, attempt to manually allocate memory
    try {
        tmp = (Symbol*) malloc(sizeof(Symbol) * symbolCount);
    }catch (const bad_alloc& e) {
        cout << "Failed to allocate memory : TMP SymbolTable" <<endl;
        return nullptr;
    }

    try{
        for (int i = 0; i < symbolCount - 1; i++) {
            tmp[i].symbol   =   (char *) malloc(sizeof(char)*STR_SIZE);
            strcpy(tmp[i].symbol,symbolTable[i].symbol);
            tmp[i].address     =   symbolTable[i].address;
        }
    }   catch (const bad_alloc& ee){
        cout << "Failed to allocate memory : TMP SymbolTable" << endl;
        return nullptr;
    }

    for (int i = 0; i < symbolCount - 1; i++) {
        free(symbolTable[i].symbol);
    }
    free(symbolTable);

    try{
        tmp[symbolCount - 1].symbol  = (char *) malloc(sizeof(char)*STR_SIZE);
        strcpy(tmp[symbolCount - 1].symbol, s);
        tmp[symbolCount - 1].address    = nextAddress;

    }   catch (const bad_alloc& ee){
        cout << "Failed to allocate memory : TMP SymbolTable" << endl;
        return nullptr;
    }

    nextAddress     +=  ADDR_INC;
    symbolTable     =   tmp;

    return  &this->symbolTable[this->symbolCount-1];
}


