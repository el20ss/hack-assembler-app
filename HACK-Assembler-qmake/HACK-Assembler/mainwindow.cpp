#include    "mainwindow.h"
#include    "./ui_mainwindow.h"
#include    <qapplication.h>
#include    <QFileDialog>
#include    <QMessageBox>

QString openFile;
QString binaryFile;
QString flashLocation;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),ui(new Ui::MainWindow){
    ui->setupUi(this);

    ui->pushButton_detect-> setEnabled(false);
    ui->pushButton_flash->  setEnabled(false);
}


MainWindow::~MainWindow(){
    delete ui;
}

//**************************************************ACTIONS******************************************************************************

void MainWindow::on_actionExit_triggered()  {
    QApplication::quit();
}


void MainWindow::on_actionNew_File_triggered()
{
    ui->ReadAssembly->clear();
}


void MainWindow::on_actionOpen_File_triggered()
{
    openFile    =   QFileDialog::getOpenFileName(this,
                    "Open .asm file",
                    windowFilePath(),
                    "HACK Assembly File (*.asm);;");

    QFile fileread(openFile);

    if(!fileread.open(QFile::Text|QFile::ReadOnly)){
        QMessageBox::warning(this,"Error","File Cannot Be Opened");
        return;
    }

    QTextStream in(&fileread);
    ui->ReadAssembly->setText(in.readAll());
}


void MainWindow::on_actionSave_asm_File_triggered()
{
    if(openFile.isEmpty()){
        openFile    =   QFileDialog::getSaveFileName(this,
                            "Save .asm file",
                            windowFilePath(),
                            "HACK Assembly File (*.asm);;");
    }

    if (openFile.isEmpty())
        return;

    QFile filesave(openFile);

    if (!filesave.open(QFile::WriteOnly | QFile::Text))
        return;

    QTextStream out(&filesave);

    out << ui->ReadAssembly->toPlainText() << "\n";

    filesave.close();
}


void MainWindow::on_actionSave_As_asm_File_triggered()
{
    openFile    =   QFileDialog::getSaveFileName(this,
                            "Save .asm file",
                            windowFilePath(),
                            "HACK Assembly File (*.asm);;");


    if (openFile.isEmpty())
        return;

    QFile filesave(openFile);

    if (!filesave.open(QFile::WriteOnly | QFile::Text))
        return;

    QTextStream out(&filesave);

    out << ui->ReadAssembly->toPlainText() << "\n";

    filesave.close();
}

//**************************************************BUTTONS******************************************************************************

void MainWindow::on_pushButton_detect_clicked()
{
    if(openFile.isEmpty()){
        return;
    }

    flashLocation       =   openFile.left(openFile.lastIndexOf(":"));
    flashLocation.append(R"(:\)");
    flashLocation       =   locateFile(flashLocation,"ROM32K_.v");

    if(!flashLocation.isNull()){
        return;
    }

    QMessageBox::warning(this,"Error","Flash Location Could Not Be Found");
    flashLocation   =   QFileDialog::getExistingDirectory(this,
                                                     "Locate ROM32K",
                                                     windowFilePath());

    return;

}


void MainWindow::on_pushButton_flash_clicked()
{

//    if (QFile::exists(flashLocation))
//    {
//        QFile::remove(flashLocation);
//    }

    QFile::copy(binaryFile, flashLocation.left(flashLocation.lastIndexOf(R"(/)")));
}


void MainWindow::on_pushButton_assemble_clicked()
{
    if(openFile.isEmpty()){
        QMessageBox::warning(this,"Error","Please save or open an existing file");
        return;
    }

    on_actionSave_asm_File_triggered();

    try{
        Assembler*  assembler   =   new Assembler(openFile.left(openFile.lastIndexOf(".")).toStdString());
        assembler->parse();
        delete(assembler);
    }   catch(const char*   err){
        QMessageBox::warning(this,"Error",err);
    }


    binaryFile      =   openFile.left(openFile.lastIndexOf("."));
    binaryFile.append(".bin");

    QFile binaryread(binaryFile);

    if(!binaryread.open(QFile::Text|QFile::ReadOnly)){
        QMessageBox::warning(this,"Error","File Cannot Be Opened");
    }

    QTextStream out(&binaryread);
    ui->WriteBinary->setText(out.readAll());

    ui->pushButton_detect-> setEnabled(true);
    ui->pushButton_flash->  setEnabled(true);
}


QString MainWindow::locateFile(QString dir, QString find)
{
    if (dir.isEmpty() || find.isEmpty()) {
        return {};
    }

//    std::string tmp;

//    for(const auto & path : std::filesystem::directory_iterator(dir)){
//        if(path.path().string().find(find) != std::string::npos){
//            return path.path().string();
//        }

//        if(path.is_directory()){
//            tmp =   locateFile(path.path().u8string(),find);
//            if(!tmp.empty()){
//                return tmp;
//            }
//        }
//    }

    QDirIterator path(dir,
                      QStringList() << "*.v",
                      QDir::Files,
                      QDirIterator::Subdirectories);

    QString tmp;
    while(path.hasNext()){
        tmp =   path.next();
        if(tmp.contains(find)){
            return tmp;
        }
    }

    return {};
}

